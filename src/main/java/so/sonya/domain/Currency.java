package so.sonya.domain;

import lombok.Getter;

@Getter
public enum Currency {
    // Regular currencies
    AED,
    AMD,
    AUD,
    AZN,
    BGN,
    BRL,
    BYN,
    CAD,
    CHF,
    CNY,
    CZK,
    DKK,
    EGP,
    EUR,
    GBP,
    GEL,
    HKD,
    HUF,
    IDR,
    INR,
    JPY,
    KGS,
    KRW,
    KZT,
    MDL,
    NOK,
    NZD,
    PLN,
    QAR,
    RON,
    RSD,
    RUB,
    SEK,
    SGD,
    THB,
    TJS,
    TMT,
    TRY,
    UAH,
    USD,
    UZS,
    VND,
    XDR,
    ZAR,

    // Cryptocurrencies
    BTC,
    ETH,
    BNB,
    XRP,
    USDT
}
