package so.sonya.domain.request;

import lombok.*;
import so.sonya.domain.Currency;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class RatesRequest {
    private Currency base;

    @Singular
    private List<Currency> symbols;
}
