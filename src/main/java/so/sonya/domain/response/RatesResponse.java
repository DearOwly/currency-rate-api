package so.sonya.domain.response;

import lombok.*;
import so.sonya.domain.Currency;

import java.util.Date;
import java.util.Map;

@NoArgsConstructor
@Data
public class RatesResponse {
    private Date date;
    private Long timestamp;
    private Currency base;
    private Map<Currency, Double> rates;
}
