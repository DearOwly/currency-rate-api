package so.sonya;

import so.sonya.client.CurrencyRateRestClient;
import so.sonya.client.impl.CurrencyRateRestClientImpl;
import so.sonya.domain.request.RatesRequest;
import so.sonya.domain.response.RatesResponse;

public class Main {
    public static void main(String[] args) {
        CurrencyRateRestClient client = new CurrencyRateRestClientImpl("http://localhost:8080");

        RatesRequest request = RatesRequest.builder().build();

        RatesResponse response = client.getLatestRates(request);

        System.out.println(response);
    }
}
