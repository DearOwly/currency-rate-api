package so.sonya.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import so.sonya.domain.Currency;
import so.sonya.domain.response.RatesResponse;

import java.util.List;

public interface CurrencyRateApiService {
    @GET("/api/latest")
    Call<RatesResponse> getLatestRates(@Query("base") Currency base, @Query("symbols[]") List<Currency> symbols);
}
