package so.sonya.service;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;

public class CurrencyRateServiceGenerator {
    private static final OkHttpClient.Builder HTTP = new OkHttpClient.Builder();

    private static final Retrofit.Builder BUILDER =
            new Retrofit.Builder()
                    .addConverterFactory(JacksonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
        return BUILDER
                .baseUrl(baseUrl)
                .build()
                .create(serviceClass);
    }

    public static <T> T executeSync(Call<T> call) {
        try {
            Response<T> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
