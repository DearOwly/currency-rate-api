package so.sonya.client.impl;

import so.sonya.client.CurrencyRateRestClient;
import so.sonya.domain.request.RatesRequest;
import so.sonya.domain.response.RatesResponse;
import so.sonya.service.CurrencyRateApiService;

import static so.sonya.service.CurrencyRateServiceGenerator.createService;
import static so.sonya.service.CurrencyRateServiceGenerator.executeSync;

public class CurrencyRateRestClientImpl implements CurrencyRateRestClient {
    private final CurrencyRateApiService currencyRateApiService;

    public CurrencyRateRestClientImpl(String baseUrl) {
        currencyRateApiService = createService(CurrencyRateApiService.class, baseUrl);
    }

    @Override
    public RatesResponse getLatestRates(RatesRequest request) {
        return executeSync(currencyRateApiService.getLatestRates(request.getBase(), request.getSymbols()));
    }
}
