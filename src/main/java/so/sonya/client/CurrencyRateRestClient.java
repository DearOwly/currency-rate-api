package so.sonya.client;

import so.sonya.domain.request.RatesRequest;
import so.sonya.domain.response.RatesResponse;

public interface CurrencyRateRestClient {
    RatesResponse getLatestRates(RatesRequest request);
}
