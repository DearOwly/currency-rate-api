# Currency Rate API

## Установка
1. Установить библиотеку в локальный репозиторий Maven, выполнив `gradle publishToMavenLocal`.
2. Добавить локальный репозиторий Maven в качестве источника зависимостей в `build.gradle`:
    ```
    repositories {
        mavenCentral()
        mavenLocal()
    }
    ```
3. Добавить зависимость в `build.gradle`: 
    ```
    implementation 'so.sonya:currency-rate-api:1.0.0'
    ```

## Использование

### Начало работы

```java
CurrencyRateApiClient client = new CurrencyRateApiClientImpl("http://example.com");
```

Вместо `http://example.com` необходимо указать URL сервиса.

Например, если Currency Rate Service крутится в Докере на порту 8081, URL будет `http://localhost:8081`.

### Получить курс валют &mdash; `RatesResponse getLatestRates(RatesRequest)`

Возвращает курс исходной валюты к целевым (сколько каждой из целевых валют можно купить за одну единицу исходной).

- Стандартное использование:
   ```java
   RatesRequest request = RatesRequest.builder()
                                      .base(Currency.RUB)   // Исходная валюта
                                      .symbols(List.of(     // Целевые валюты      
                                           Currency.USD,
                                           Currency.EUR,
                                           Currency.BTC     // Можно смешивать обычные валюты и криптовалюты
                                      ))   
                                      .build();
   
   RatesResponse response = client.getLatestRates(request);
  
   System.out.println(response);
   ```
- Целевые валюты можно задавать по одной:
   ```java
   RatesRequest request = RatesRequest.builder()
                                      .base(Currency.RUB)   // Исходная валюта
                                      .symbol(Currency.USD) // Целевые валюты
                                      .symbol(Currency.EUR)
                                      .symbol(Currency.BTC)
                                      .build();
   
   RatesResponse response = client.getLatestRates(request);
  
   System.out.println(response);
   ```
- Можно не указывать исходную валюту. Значение по умолчанию &mdash; `Currency.USD`:
   ```java
   RatesRequest request = RatesRequest.builder()
                                      .symbols(List.of(    // Целевые валюты      
                                           Currency.USD,
                                           Currency.EUR,
                                           Currency.BTC
                                      )) 
                                      .build();
   
   RatesResponse response = client.getLatestRates(request);
  
   System.out.println(response);
   ```
- Можно не указывать целевые валюты. Тогда в ответе будут все известные:
   ```java
   RatesRequest request = RatesRequest.builder()
                                      .build();
   
   RatesResponse response = client.getLatestRates(request);
  
   System.out.println(response);
   ```

## Дополнительно

* Список поддерживаемых валют можно найти в `so.sonya.domain.Currency`.